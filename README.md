---
title: Linux Projects. Raspberry Pi 4 & Open Media Vault NAS. Οδηγός για αρχάριους στα ελληνικά.
description: Open media Vault NAS. Installation on a Raspberry Pi 4. Beginners guide in Greek.
creator: Nikas, Konstantinos N.
date: 26/02/2021
language: el



---

# Linux Projects. Raspberry Pi 4 & Open Media Vault NAS. Οδηγός για αρχάριους στα ελληνικά.

------







## Εισαγωγή

### Οδηγός για αρχάριους

Στον οδηγό που ακολουθεί, επιχειρείται μία απλή περιγραφή της εγκατάστασης του Open Media Vault στο Raspberry Pi 4.

### Συσκευές

Οι συσκευές που χρησιμοποιήθηκαν είναι:

1. Ένα  Raspberry Pi 4 με μνήμη 4gb RAM και η συσκευή τροφοδοσίας του
2. Ένα καλώδιο ethernet για την σύνδεση του Raspberry Pi στο router του τοπικού δικτύου
3. Μία κάρτα microSD 16gb με τον υποδοχέα της για την τοποθέτηση σε υπολογιστή και έπειτα στο Rpi4
4. Ένας υπολογιστής για έλεγχο του Rpi4 με το πρωτόκολλο ssh

### Λειτουργικά συστήματα

Τα λειτουργικά συστήματα που χρησιμοποιήθηκαν είναι:

1. Raspberry Pi OS Lite  βασισμένο στο Debian Linux OS ως αρχικό λειτουργικό σύστημα για το Rpi4

2. Open Media Vault βασισμένο στο Debian Linux OS ως τελικό λειτουργικό σύστημα του Rpi4, από το αποθετήριο με διεύθυνση https://github.com/OpenMediaVault-Plugin-Developers/installScript

3. Pop!_OS 20.04 by System76 βασισμένο στο Ubuntu 20.04 Linux (Unix-like) OS ως λειτουργικό σύστημα του φορητού υπολογιστή από τον οποίο έγινε ο έλεγχος του Rpi4.

   

### Open Media Vault

Το [Open Media Vault](https://blog.openmediavault.org/)  αποτελεί μια λύση αποθήκευσης ψηφιακών πόρων, διαμοιρασμού αρχείων και υπηρεσιών του διαδικτύου.

Βασίζεται στο λειτουργικό **Debian Linux OS**.

 Απευθύνεται σε οικιακούς χρήστες ή σε μικρές επιχειρήσεις.

Το μεγάλο του πλεονέκτημα είναι η εύκολη εγκατάσταση του σε συνδυασμό με την πληθώρα των υπηρεσιών που περέχει, μέσα από λειτουργίες και πρόσθετα, καθώς και η ευκολία χρήσης του.

### Raspberry Pi 4

Το [Raspberry Pi 4](https://www.raspberrypi.org/products/raspberry-pi-4-model-b/) (Rpi4) είναι ένας μικρός υπολογιστής στο μέγεθος πιστωτικής κάρτας, ο οποίος έχει μικρή κατανάλωση ενέργειας και ο χειρισμός του μπορεί να πραγματοποιηθεί χωρίς γραφικό περιβάλλον, από άλλον υπολογιστή αξιοποιώντας το δικτυακό πρωτόκολλο ssh (Secure Shell).

## Λέξεις-Κλειδιά

Open Media Vault NAS, Raspberry Pi 4, Linux



## Συντομογραφίες

OMV: Open Media Vault

NAS: Network-Attached Storage

Rpi: Raspberry Pi 

SSH: Secure Shell

IP: Internet Protocol

MAC: Media Access Control

OS: Operating System

HDD: Hard Disc Drive

SSD: Solid State Drive

SD: Secure Digital

USB: Universal Serial Bus

ISP: Internet Service Provider

WLAN: Wireless LAN

LAN: Local Area Network

DHCP: Dynamic Host Configuration Protocol

## Σκοπός

Ο σκοπός του οδηγού αυτού είναι η κατανοητή περιγραφή των διαδικασιών εγκατάστασης και δημιουργίας ενός σταθμού Open Media Vault NAS, σε ένα Rpi4, παρέχοντας στο δημιουργό του και τους χρήστες του, πληθώρα εφαρμογών και λύσεων, σε  όλους τους τομείς ενδιαφέροντος, είτε μέσα από το τοπικό δίκτυο, είτε διαμέσω απομακρυσμένης πρόσβασης.

## Περιγραφή Εγκατάστασης

Πρώτα συνδέουμε το Raspberry Pi 4  σε μία πρίζα.

Έπειτα συνδέουμε με το καλώδιο Ethernet, το Rpi4 με το router μας στις κατάλληλες υποδοχές τους. 

Θα καθορίσουμε έπειτα και την ασύρματη σύνδεση του Rpi4 στο τοπικό δίκτυο μας.

Τοποθετούμε την κάρτα microSD με τη βοήθεια υποδοχέα στον φορητό υπολογιστή μας.

Κάνουμε εγγραφή του λειτουργικού  Raspberry Pi OS Lite στην κάρτα microSD που έχουμε τοποθετήσει στον φορητό υπολογιστή μας, με τη βοήθεια του προγράμματος Raspberry Pi Imager το οποίο έχουμε εγκαταστήσει.

Δημιουργούμε ένα κενό αρχείο με το όνομα ssh, χωρίς επέκταση στον φάκελο boot του Raspberry Pi 4 OS. 

Δημιουργούμε επίσης στον φάκελο boot, ένα αρχείο με την ονομασία wpa_supplicant.conf με τις πληροφορίες της ασύρματης σύνδεσής μας (Κωδικός χώρας, όνομα δικτύου wi-fi, κωδικός wi-fi). 

Βγάζουμε την κάρτα από τον υπολογιστή μας και την τοποθετούμε στην κατάλληλη υποδοχή του Rpi4 το οποίο έχουμε αποσυνδέσει πρώτα από το δίκτυο ηλεκτροδότησης.

Συνδέουμε πάλι τη συσκευή Rpi4. 

Ο λαμπτήρας led της συσκευής αλλάζει χρώμα από πράσινο σε κόκκινο. 

Κάνουμε ένα μικρό διάλειμμα, περιμένουμε λίγα λεπτά της ώρας (περίπου 3 min). 

Το λειτουργικό σύστημα Raspberry Pi OS Lite  έχει εγκατασταθεί στο raspberry.

Συνδεόμαστε από τον φορητό υπολογιστή μας με το Rpi4 με το πρωτόκολλο ssh. 

Αλλάζουμε τον προκαθορισμένο κωδικό του Rpi4, ο οποίος αρχικά είναι ο raspberry  για τον χρήστη pi, για λόγους ασφαλείας.

Εγκαθιστούμε το νέο μας λειτουργικό σύστημα, το Open Media Vault 5 στο Rpi4,  με τη βοήθεια ενός απλού  installation script.  

Κάνουμε άλλο ένα διάλειμμα, παρακολουθώντας την εγκατάσταση από την οθόνη του υπολογιστή μας.

Συνδεόμαστε από οποιαδήποτε συσκευή, windows, android, ios στο τοπικό δικτυό μας στη διεύθυνση του Rpi4 στον φυλλομετρητή μας και κάνουμε τις αρχικές ρυθμίσεις στον  δικό μας OpenMediaVault NAS, με τους κωδικούς χρήστη: admin και κωδικό password: openmediavault, τους οποίους πρέπει να αλλάξουμε μετά την πρώτη σύνδεση μας.  

Μπορούμε να συνδέσουμε στη συσκευή μας Rpi4 (πλέον OpenMedia Vault NAS), οποιαδήποτε μορφή HDD, SSD, SD, USBstick, για την επέκταση του αποθηκευτικού χώρου μας, οποιασδήποτε χωρητικότητας, διαδικασία η οποία όμως δεν περιγράφεται σε αυτόν τον οδηγό εγκατάστασης.

## Περιγραφή με εικόνες



### Εγκατάσταση Raspberry Pi Imager στον υπολογιστή

Εγκαθιστούμε το λογισμικό Raspberry Pi Imager (Rpi Imager) στον υπολογιστή (από τον οποίο θα ελέγχουμε τη συσκευή Rpi4 στο δίκτυο μας), από τις ηλεκτρονικές διευθύνσεις

https://www.raspberrypi.org/%20downloads/

https://downloads.raspberrypi.org/imager/imager_1.4_amd64.deb .



![Imager0](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/Imager0.png)



Χρησιμοποιούμε το λογισμικό εγκατάστασης πακέτων με κατάληξη .deb του λειτουργικού μας συστήματος, στην περίπτωσή μας τον Eddy (package installer).

![Imager01](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/Imager01.png)



Μπορούμε εναλλακτικά να εγκαταστήσουμε το Rpi Imager στον υπολογιστή μας, από το κατάστημα λογισμικού του συστήματος μας. Στην περίπτωσή μας είναι το pop!-shop.

![Imager02](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/Imager02.png)

### Εγγραφή του Raspberry Pi OS Lite σε κάρτα microSD

Η διαδικασία εγγραφής του λειτουργικού συστήματος στην κάρτα SD, είναι απλή και γίνεται σε τρία βήματα. Πρώτον επιλέγουμε το επιθυμητό λειτουργικό σύστημα, δεύτερον επιλέγουμε την κάρτα SD στην οποία θα γίνει η εγγραφή και τρίτον κάνουμε την εγγραφή με τη βοήθεια του λογισμικού  Rpi Imager.

![Imager1](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/Imager1.png)



Αναλυτικότερα, πατώντας το "πλήκτρο" **CHOOSE OS**, επιλέγουμε από το πεδίο **Raspberry Pi OS (other)**, του λογισμικού Rpi Imager,

![Imager2](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/Imager2.png)



το **Raspberry Pi OS Lite (32bit)**, το οποίο δεν απαιτεί μεγάλη κατανάλωση πόρων και δεν έχει γραφικό περιβάλλον.

![Imager3](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/Imager3.png)



Έπειτα επιλέγουμε το μέσο εγγραφής(**CHOOSE SD CARD**), δηλαδή την microSD με τον αντάπτορα της μέσα στον υπολογιστή μας.

![Imager4](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/Imager4.png)



χωρητικότητας τουλάχιστον 16 GB.

![Imager5](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/Imager5.png)



Προχωράμε στη διαδικασία της εγγραφής (**WRITE**).

![Imager6](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/Imager6.png)


Δίνουμε βάση στα προειδοποιητικά μηνύματα, για την επικείμενη διαγραφή τυχόν περιεχομένου της κάρτας microSD.

![Imager8](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/Imager8.png)



Ακολουθεί η εγκατάσταση του επιθυμητού λειτουργικού συστήματος στην εσωτερική κάρτα  SD. 

Έπειτα μπορούμε να απομακρύνουμε την κάρτα microSD με τον αντάπτορα της από τον υπολογιστή μας.

![Imager9](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/Imager9.png)



### Εγκατάσταση ssh και ρυθμίσεων ασύρματου δικτύου.

Τοποθετούμε πάλι την microSD με τον αντάπτορα της στον υπολογιστή μας και ανοίγουμε τον φάκελο **/boot** με δεξί κλικ στο τερματικό μας, επιλέγοντας **open in terminal**.

![Imager11](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/Imager11.png)







![Imager12](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/Imager12.png)



Δημιουργούμε το αρχείο **ssh** χωρίς κάποια επέκταση, με την εντολή: 

```bash
touch ssh
```

στη "διαδρομή"  ``/media/<όνομα χρήστη>/boot``  στην οποία βρισκόμαστε.

![omv13](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv13.png)





![omv14](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv14.png)





Επίσης δημιουργούμε το αρχείο **wpa_supplicant.conf** σύμφωνα με το υπόδειγμα της σελίδας https://www.raspberrypi.org/documentation/configuration/wireless/headless.md  με τις δικές μας ρυθμίσεις, κωδικό χώρας, όνομα wi-fi δικτύου, κωδικό wi-fi, από την γραμμή εντολών πληκτρολογώντας

```bash
 sudo nano wpa_supplicant.conf
```

επίσης στο μονοπάτι του **/boot**.

![omv15](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv15.png)



με περιεχόμενο:

```json
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=GR

network={
 ssid="ονομασία δικτύου wi-fi"
 psk="κωδικός δικτύου wi-fi "
}
```

αλλάζοντας τις τιμές στα τρία πεδία:

``country=``

``ssid="  "``

``psk="  "``

με τον κωδικό της χώρας μας, την ονομασία wi-fi και τον κωδικό του wi-fi μας.

![omv16](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv16.png)



και αποθηκεύουμε τις αλλαγές με το συνδιασμό των πλήκτρων ``Ctrl``+``O`` και έπειτα ``Ctrl``+ ``X``.

![omv17](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv17.png)



### Σύνδεση υπολογιστή με  Raspberry Pi 4

Βγάζουμε την κάρτα από τον υπολογιστή μας και την τοποθετούμε στην κατάλληλη υποδοχή του Rpi4 το οποίο έχουμε αποσυνδέσει πρώτα από το δίκτυο ηλεκτροδότησης.
Συνδέουμε πάλι τη συσκευή Rpi4.
Ο λαμπτήρας led της συσκευής αλλάζει χρώμα από πράσινο σε κόκκινο.
Κάνουμε ένα μικρό διάλειμμα, περιμένουμε λίγα λεπτά της ώρας (περίπου 3 min).
Το λειτουργικό σύστημα Raspberry Pi OS Lite  έχει εγκατασταθεί στο raspberry.

Για να βρούμε την διέυθυνση ip της συσκευής Rpi4, χρησιμοποιούμε μία από τις εφαρμογές σάρωσης θυρών του τοπικού δικτύου που κυκλοφορούν για όλα τα λειτουργικά συστήματα. 

Είτε τη διεπαφή του δρομολογητή μας, όπως περιγράφεται στο τέλος του σύντομου αυτού οδηγού.

Το όνομα της συσκευής θα εμφανίζεται ως raspberrypi  και θα δίνονται επίσης πληροφορίες για τις διευθύνσεις  MAC και IP.  

H IP διεύθυνση είναι περίπου της μορφής 192.168.xxx.xxx . 

Στην περίπτωσή μας χρησιμοποιήθηκε η android εφαρμογή Port Authority από το F-Droid, ή το Google Play Store. 

Μάλιστα θα εμφανίζονται δύο hosts με την ονομασία raspberrypi,  η πρώτη αφορά την ενσύρματη δικτύωση και η δεύτερη την ασύρματη για την ίδια συσκευή.

Για να συνδεθούμε από τον υπολογιστή μας στο Rpi4, πληκτρολογούμε στο τερματικό του υπολογιστή μας, διαδοχικά τις εντολές:

```bash
cd
```

```bash
ssh pi@192.168.xxx.xxx
```

, όπου 192.168.xxx.xxx η ip διεύθυνση του Rpi4, που βρήκαμε όπως περιγράψαμε παραπάνω.

![omv18](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv18.png)



Επιβεβαιώνουμε την πρώτη φορά ότι επιθυμούμε να συνδεθούμε διαμέσω ssh.

``yes``

![omv19](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv19.png)



Πληκτρολογούμε τον προκαθορισμένο κωδικό του Rpi4, ο οποίος είναι **raspberry**.

![omv20](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv20.png)



**Έχουμε πλέον συνδεθεί στη συσκευή Rpi4, από τον υπολογιστή μας.**

Για λόγους ασφαλείας αλλάζουμε τον κωδικό πρόσβασης,  με την εντολή ``passwd`` , 

```bash
pi@raspberrypi:~ $ passwd
```

![omv21](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv21.png)





![omv22](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv22.png)



### Εγκατάσταση του OpenMediaVault installation script στο Rpi4

Η εγκατάσταση πλέον του Open Media Vault στη συσκευή Rpi4 είναι πολύ απλή. Πρώτα εκτελούμε τα απαραίτητα βήματα update, upgrade, όπως σε κάθε εγκατάσταση.

![omv23](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv23.png)



![omv24](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv24.png)



Έπειτα από το αποθετήριο https://github.com/OpenMediaVault-Plugin-Developers/installScript, ακολουθούμε τις οδηγίες εγκατάστασης, σε τρία πολύ απλά βήματα, όπως φαίνεται στο παρακάτω στιγμιότυπο.

```bash
wget https://github.com/OpenMediaVault-Plugin-Developers/installScript/raw/master/install

chmod +x install

sudo ./install -n
```



![omv26](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv26.png)



Και περιμένουμε πλέον, υπομονετικά, για αρκετά λεπτά της ώρας, παρακολουθώντας την εγκατάσταση του Open Media Vault στο Raspberry Pi 4, από την οθόνη του υπολογιστή μας.

![omv27](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv27.png)





![omv28](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv28.png)



Αφού τελειώσει η εγκατάσταση, εκτελούμε επανεκκίνηση του Raspberry Pi 4 

```bash
pi@raspberrypi:~ $ sudo reboot
```

και περιμένουμε για λίγο να ολοκληρωθεί.

![omv29](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv29.png)



### Σύνδεση με τον Open Media Vault NAS

Η εγκατάσταση μας έχει πλέον ολοκληρωθεί και είμαστε έτοιμοι να συνδεθούμε στον δικό μας Open Media Vault NAS.

Από μία συσκευή στο δίκτυο μας, πληκτρολογούμε στο πεδίο αναζήτησης διευθύνσεων του φυλλομετρητή μας, τη διεύθυνση του raspberrypi:  ``http://ip του raspberrypi``, όπου η ip του raspberrypi θα είναι περίπου της μορφής 192.168.xxx.xxx, ίδια με αυτή που χρησιμοποιήσαμε για τη σύνδεση με το πρωτόκολλο ssh.

Οι προκαθορισμένοι κωδικοί σύνδεσης είναι

 **Username** ``admin``

**Password** ``openmediavault``



![omv30](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv30.png)



### Αρχικές ρυθμίσεις του Open Media Vault NAS

Μετά την είσοδο μας στον πίνακα ελέγχου του OMV, βλέπουμε τις υπηρεσίες(**Services**), οι οποίες, είτε είναι ενεργοποιημένες, είτε εκτελούνται στο σύστημα μας με πράσινη ένδειξη, καθώς και τις πληροφορίες του συστήματος μας(**System Information**).

![omv31](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv31.png)



Για να αποφύγουμε το παρακάτω προειδοποιητικό μήνυμα, το οποίο δεν πρέπει να μας ανησυχεί καθόλου, γιατί σημαίνει ότι έχουν περάσει πέντε λεπτά από την είσοδο μας στο σύστημα,

![omv32](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv32.png)



πηγαίνουμε στις γενικές ρυθμίσεις(**General Settings**) και στο πεδίο **Auto logout**, αλλάζουμε τον χρόνο από 5 λεπτά, σε 30 λεπτά ή μία ημέρα, ώστε να μη διακόπτεται η συνεδρία από την αυτόματη αποσύνδεση.

![omv33](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv33.png)



Επίσης πολύ βασική ρύθμιση, είναι η αλλαγή του password,  από τις γενικές ρυθμίσεις(**General Settings**), στο πεδίο **Web Administrator Password**.

Μετά από κάθε ρύθμιση που κάνουμε πατάμε πάντα τα πλήκτρα **Save**, **Apply**.

![omv35](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv35.png)





![omv36](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv36.png)





![omv37](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv37.png)



Στο πεδίο **Date & Time** από το αριστερό μενού, επιλέγουμε την τοπική ώρα, στην περίπτωσή μας την **Europe/Athens**.

![omv38](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv38.png)





![omv39](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv39.png)



Πάντα επιβεβαιώνουμε μετά από κάθε ρύθμιση με **Save**, **Apply**.

![omv40](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv40.png)





![omv41](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv41.png)





![omv42](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv42.png)



Για να αλλάξουμε τη γλώσσα του συστήματός μας από Αγγλικά σε Ελληνικά, επιλέγουμε το πεδίο **Language** του menu του **raspberrypi**, στην πάνω δεξιά γωνία της οθόνης μας.

![omv43](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv43.png)







![omv44](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv44.png)







![omv45](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv45.png)



### Δημιουργία στατικής ip διεύθυνσης της συσκευής μας

Επειδή η διεύθυνση ip της συσκευής μας δεν είναι στατική και αλλάζει, αυτό θα έχει ως αποτέλεσμα την αδυναμία σύνδεσης στη συσκευή μας, όταν συμβεί η αλλαγή αυτή.

Μπορούμε επομένως να καθορίσουμε μία στατική διεύθυνση για τη συσκευή μας από το γραφικό περιβάλλον του δρομολογητή του παρόχου μας.

Στην περίπτωσή μας, συνδεθήκαμε με το γραφικό περιβάλλον του Speedport Entry 2i του παρόχου μας ISP(Cosmote), στη διεύθυνση η οποία είναι συνήθως η http://192.168.1.1 με τους κωδικούς admin, password, οι οποίοι αναγράφονται στο κάτω μέρος της βάσης του δρομολογητή μας, οι οποίοι είναι διαφορετικοί από τους κωδικούς του wi-fi.

![omv46](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv46.png)



 Στη σελίδα **Home**, βλέπουμε την ασύρματη(WLAN) και την ενσύρματη(LAN) διεύθυνση σύνδεσης της συσκευής που μας ενδιαφέρει, στην περίπτωσή μας, του **raspberrypi**, τις οποίες θα καθορίσουμε ως στατικές IP διευθύνσεις.

![omv47](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv47.png)



Στη σελίδα **Local Network**, θα χρησιμοποιήσουμε τα menu:  

**Allocated Address (DHCP)**  και **DHCP Binding**.

![omv48](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv48.png)



Από το πρώτο menu, **Allocated Address (DHCP)** θα αντλήσουμε όλες τις πληροφορίες για τις συνδεμένες συσκευές στο δίκτυο μας και τις διευθύνσεις τους MAC, IP και ιδιαίτερα τις δύο διευθύνσεις του raspberrypi.

![omv49](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv49.png)



Στο αναδυόμενο μενού του DHCP Binding επιλέγουμε Create New Item, από το τέλος της σελίδας και στη συνέχεια συμπλήρώνουμε τις διευθύνσεις MAC, IP του raspberrypi μας και δίνουμε μία ονομασία στη συσκευή μας.

Επαναλαμβάνουμε την ίδια διαδικασία και για την ασύρματη σύνδεση και πάντα επιβεβαιώνουμε πατώντας το πλήκτρο Apply.

Με αυτόν τον τρόπο δημιουργήσαμε στατικές διευθύνσεις για τη συσκευή raspberrypi(Rpi4) μας, ώστε να συνδεόμαστε απρόσκοπτα στο λειτουργικό μας σύστημα του  Open Media Vault NAS.

![omv50](https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/raw/master/screenshots/omv50.png)



 Σε αυτό το σημείο ολοκληρώθηκε ο οδηγός εγκατάστασης του Open Media Vault NAS, στο  Raspberry Pi 4, με τη βοήθεια του πρωτοκόλλου ssh, από άλλον υπολογιστή.

Είμαστε πλέον έτοιμοι να εγκαταστήσουμε τις υπηρεσίες και τις εφαρμογές διαδικτύου που επιθυμούμε από το τοπικό μας δίκτυο.

Καλή επιτυχία!

## Παραπομπές

https://www.openmediavault.org/

https://blog.openmediavault.org/

https://www.raspberrypi.org/

https://www.raspberrypi.org/%20downloads/

https://downloads.raspberrypi.org/imager/imager_1.4_amd64.deb

https://www.raspberrypi.org/documentation/configuration/wireless/headless.md 

https://github.com/OpenMediaVault-Plugin-Developers/installScript

https://images.unsplash.com/photo-1504604361470-ff454462c1da?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80

https://gitlab.com/nikaskonstantinos/rpi4_omv_el_nb_guide/-/edit/master/README.md








